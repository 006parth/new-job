import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { JobsService } from '../services/jobs.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-joblist',
  templateUrl: './joblist.component.html',
  styleUrls: ['./joblist.component.scss']
})
export class JoblistComponent implements OnInit {
  keyword: any;
  location: any;
  ip: any = "";
  total: any;
  jobsList: any;
  totalPages: any;
  loading: boolean = false;
  pagination: any;
  start: any = 0;
  select: boolean = false;
  filter: any = 'r';
  open: boolean = false;
  dataModel: any;

  constructor(public route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    public router: Router, public dataService: DataService, public jobService: JobsService) {
    if (localStorage.getItem('loc')) {
      if (localStorage.getItem('loc') && localStorage.getItem('key')) {
        this.keyword = localStorage.getItem('key');
        this.location = localStorage.getItem('loc');
        this.getIpAddress();
      } else {
        this.location = localStorage.getItem('loc');
        this.getIpAddress();
      }
    }
  }
  goHome() {
    this.select = true;
    setTimeout(() => {
      this.router.navigate(['']);
    }, 700);
  }

  ngOnInit() {
  }
  menuClick() {
    if (this.open) {
      this.open = false;
    } else {
      this.open = true;
    }
  }

  getIpAddress() {
    this.loading = true;
    this.jobService.getIp().subscribe((res) => {
      this.ip = res.ip;
      if (this.keyword || this.location) {
        this.findJobs();
      }
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    })
  }

  setFilter(type) {
    if (this.filter === type) {

    } else {
      this.filter = type;
      this.findJobs();
    }
  }

  findJobs() {
    if (this.keyword || this.location) {
      localStorage.setItem('loc', this.location);
      localStorage.setItem('key', this.keyword);
      this.loading = true;
      let data = {
        key: this.keyword,
        loc: this.location,
        ip: this.ip,
        sort: this.filter
      }
      this.jobService.findJobs(data).subscribe((res) => {
        this.total = res.total;
        this.jobsList = res.jobs;
        this.totalPages = Math.ceil(this.total / 10);
        this.loading = false;
        this.pagination = this.getPager(this.total, 1, 10);
      }, err => {
        console.log(err.message);
        this.loading = false;
        this.openSnackBar(err.message);
      })
    }
  }
  loadMore() {
    this.loading = true;
    this.jobsList = [];
    let data = {
      key: this.keyword,
      loc: this.location,
      ip: this.ip,
      sort: this.filter,
      start: this.start
    }
    this.jobService.loadMore(data).subscribe((res) => {
      this.total = res.total;
      this.jobsList = res.jobs;
      this.loading = false;
    }, err => {
      this.loading = false;
      this.openSnackBar(err.message);
    })
  }
  openSnackBar(message: string) {
    this._snackBar.open(message, 'Close', {
      duration: 2000,
    });
  }

  apply(job) {
    // this.router.navigate(['jobdetail', job.url ]);
    window.open(job.url);
  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    let totalPages = Math.ceil(totalItems / pageSize);
    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > totalPages) {
      currentPage = totalPages;
    }

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }
  setPage(page: number) {
    this.pagination = this.getPager(this.total, page);
    let startValue = page - 1
    if (startValue) {
      this.start = startValue * 10;
      this.loadMore();
    } else {
      this.start = 0;
      this.loadMore();
    }
  }
  openNav() {
    this.open = true;
    document.getElementById("mySidenav").style.width = "250px";
  }
  closeNav() {
    this.open = false;
    document.getElementById("mySidenav").style.width = "0";
  }
}
