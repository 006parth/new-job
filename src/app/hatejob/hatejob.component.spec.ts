import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HatejobComponent } from './hatejob.component';

describe('HatejobComponent', () => {
  let component: HatejobComponent;
  let fixture: ComponentFixture<HatejobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HatejobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HatejobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
