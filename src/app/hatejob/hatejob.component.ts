import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hatejob',
  templateUrl: './hatejob.component.html',
  styleUrls: ['./hatejob.component.scss']
})
export class HatejobComponent implements OnInit {
  open: boolean = false;

  constructor() { }

  ngOnInit() {
  }
  menuClick() {
    if (this.open) {
      this.open = false;
    } else {
      this.open = true;
    }
  }
}
