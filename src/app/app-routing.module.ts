import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobsearchComponent } from './jobsearch/jobsearch.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { HatejobComponent } from './hatejob/hatejob.component';
import { JoblistComponent } from './joblist/joblist.component';
import { ContactusComponent } from './contactus/contactus.component';
import { CookieComponent } from './cookie/cookie.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';

const routes: Routes = [
  { path: '', component: JobsearchComponent },
  { path: 'searchjob', component: JobsearchComponent },
  { path: 'hatejob', component: HatejobComponent },
  { path: 'joblist', component: JoblistComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'cookie', component: CookieComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'privacy', component: PrivacyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
