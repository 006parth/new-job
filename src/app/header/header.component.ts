import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  open: boolean = false;
  constructor(public route: ActivatedRoute) {
  }

  ngOnInit() {
  }
  menuClick() {
    if (this.open) {
      this.open = false;
    } else {
      this.open = true;
    }
  }
  openNav() {
    this.open= true;
    document.getElementById("mySidenav").style.width = "250px";
  }
  closeNav() {
    this.open = false;
    document.getElementById("mySidenav").style.width = "0";
  }



}
