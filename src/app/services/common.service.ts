import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { Observable, of, throwError } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }
  
  get(url) {
    return this.http
      .get<any>(
        url
      )
      .pipe(
        map(response => response),
        catchError(error => {
          return throwError(error);
        })
      );
      
  }
}
