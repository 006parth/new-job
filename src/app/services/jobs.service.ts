import { Injectable } from '@angular/core';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

  constructor(public commonService: CommonService) { }

  findJobs(data) {
    return this.commonService.get('https://api.jobs2careers.com/api/search.php?id=3573&pass=P7YQQNzbOdTIjbac&ip=' + data.ip + '&q=' + data.key + '&l=' + data.loc + '&sort=' + data.sort + '&format=json&link=1');
  }
  loadMore(data) {
    return this.commonService.get('https://api.jobs2careers.com/api/search.php?id=3573&pass=P7YQQNzbOdTIjbac&ip=' + data.ip + '&q=' + data.key + '&l=' + data.loc + '&sort=' + data.sort + '&format=json&start=' + data.start + '&link=1');
  }
  getIp() {
    // return this.commonService.get('https://jsonip.com');
    return this.commonService.get('https://api.ipify.org/?format=json');
  }
}
