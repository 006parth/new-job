import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-jobsearch',
  templateUrl: './jobsearch.component.html',
  styleUrls: ['./jobsearch.component.scss']
})
export class JobsearchComponent implements OnInit {
  keyword: any;
  location: any = '';
  dataModel: any;
  key: any = '';
  select: boolean = false;
  industry: any;
  sect1: any;
  sect2: any;
  isMob: boolean = false;
  sect3: any;
  // videoPlayer: HTMLVideoElement;
  @ViewChild("videoPlayer", { static: false }) videoplayer: ElementRef;
  // set mainVideoEl(el: ElementRef) {
    // this.videoPlayer = el.nativeElement;
  // }
  sect4: any;
  constructor(public dataService: DataService,public elementRef: ElementRef, public router: Router) {
    this.sect1 = this.dataService.sector1;
    this.sect2 = this.dataService.sect2;
    this.sect3 = this.dataService.sect3;
    this.sect4 = this.dataService.sect4;
    this.industry = dataService.industry;
    if (this.dataService.location) {
      this.location = this.dataService.location;
    }
  }

  ngOnInit() {
    console.log(window.innerWidth);
    if (window.innerWidth < 768) {
      this.isMob = true;
    } else {
      this.isMob = false;
    }
   
  }
  ngAfterViewInit() {
    
  }
  vidEnded(event) {
    console.log(event);
    var myVideo: any = document.getElementById("video-2");
    myVideo.currentTime = 0;
  }
  videoEnded(event) {
    console.log(event);
    var myVideo: any = document.getElementById("video");
    myVideo.currentTime = 0;
  }
  playVideo(event) {
    //   console.log(event);
    //   var style = document.createElement('play-button');
    //   style.innerHTML =
    // '.play-but {' +
    // 	'display: none;' +
    // '}';
    event.toElement.play()
  }
  play() {
    // var audioEl = document.getElementById("video");
    // audioEl.load();

    // this.videoplayer.play();
  }
  toggle(event) {
    this.select = false;
  }

  aasignValue(key) {
    window.scrollTo(0, 0);
    this.keyword = key;
  }

  findJobs() {
    this.select = true;
    setTimeout(() => {
      // this.keyword = cat;
      console.log(this.keyword);
      if (this.key || this.keyword) {
        if (this.key && this.location) {
          localStorage.setItem('loc', this.location);
          localStorage.setItem('key', this.key);
          this.dataService.keyword = this.key;
          this.dataService.location = this.location;
          this.router.navigate(['joblist']);
        }
        else if (this.keyword && this.location) {
          localStorage.setItem('key', this.keyword);
          localStorage.setItem('loc', this.location);
          this.dataService.keyword = this.keyword;
          this.dataService.location = this.location;
          this.router.navigate(['joblist']);
        }
        else if (this.location) {
          localStorage.setItem('loc', this.location);
          this.dataService.location = this.location;
          this.router.navigate(['joblist']);
        }
      }
    }, 300);
  }

}
