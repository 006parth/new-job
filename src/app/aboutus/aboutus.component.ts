import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})
export class AboutusComponent implements OnInit {
  open: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  menuClick() {
    if (this.open) {
      this.open = false;
    } else {
      this.open = true;
    }
  }


}
